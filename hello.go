package main

import (
  "fmt"
  "example.com/user/hello/morestrings"
  "github.com/google/go-cmp/cmp"
)

func main() {
  fmt.Println("Hello, world.")
  fmt.Println(morestrings.ReverseRunes("Hello, world."))
  fmt.Println(cmp.Diff("Hello World", "hello go"))
}
