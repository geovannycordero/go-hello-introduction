# How to Write Go Code Totorial

Following [this](https://golang.org/doc/code.html) tutorial, the go basics.

Install Go: [Downloads](https://golang.org/dl/)

Run ```go build``` to create a executable file. 

Or to build and install thet program with the go tool: 
```bash
go install example.com/user/hello
```
This command builds the hello command, producing an executable binary. It then installs that binary as `$HOME/go/bin/hello`.
